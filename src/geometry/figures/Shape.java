package geometry.figures;

public abstract class Shape extends Geometry {

	private double area;
	private double perimeter;

	public Shape(String name, double centerX, double centerY) {
		super(name, centerX, centerY);

	}

	public abstract double calculateArea();

	public abstract double calculatePerimeter();

}